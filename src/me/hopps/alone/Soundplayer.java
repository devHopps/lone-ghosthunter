package me.hopps.alone;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

public class Soundplayer {
	Sound gameOver, heal, hit, hurt, WeaponUse;
	
	public Soundplayer() throws SlickException {
		gameOver = new Sound("res/sound/gameover.wav");
		heal = new Sound("res/sound/heal.wav");
		hit =new Sound("res/sound/hit.wav");
		hurt =new Sound("res/sound/hurt.wav");
		WeaponUse =new Sound("res/sound/Weapon_use.wav");
		
	}
	
	public void play(String s) {
		if(s == "gameOver") {
			gameOver.play(1f, 0.3f);
		}
		if(s == "heal") {
			heal.play(0.5f,0.2f);
		}
		if(s == "hit") {
			if(!hit.playing()) {
				hit.play(0.3f,0.2f);
			}
		}
		if(s == "hurt") {
			hurt.play(0.5f,0.2f);
		}
		if(s == "WeaponUse") {
			WeaponUse.play();
		}
	}
	
}
