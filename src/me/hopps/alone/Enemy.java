package me.hopps.alone;

import java.util.Random;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

public class Enemy {
	
	double xPos, yPos, steps;
	Image Image;
	SpriteSheet Sprites;
	boolean dead = false;
	Animation hurtAnim;
	
	int Life;
	
	public Enemy(SpriteSheet ySprites,int xxPos,int yyPos, int lifes) {
		xPos = xxPos;
		yPos = yyPos;
		Sprites = ySprites;
		Image = Sprites.getSprite(0, 4);
		hurtAnim = new Animation(Sprites, 7, 0, 7, 6, true, 120, true);
		hurtAnim.setLooping(false);
		Life = lifes;
	}
	
	public void hurt(int dmg, String dir) {
		if(hurtAnim.getFrame() > 5) {
			hurtAnim.restart();
			Life -= dmg;
			if(dir == "right") {
				xPos += 30;
			}
			if(dir == "left") {
				xPos -= 30;
			}
			if(dir == "down") {
				yPos += 30;
			}
			if(dir == "up") {
				yPos -= 30;
			}
		}
	}
	
	public void update(int delta, double PlayerX, double PlayerY) {
		move(PlayerX, PlayerY, delta);
	}
	
	public void move(double PlayerX, double PlayerY, int delta) {
		Random rand = new Random();
		if(Life > 0) {
			if(PlayerX != xPos || PlayerY != yPos ) {
				if(PlayerX > xPos && rand.nextBoolean()) {
					xPos += delta/15;
				}
				else if(PlayerX < xPos && rand.nextBoolean()) {
					xPos -= delta/15;
				}
				else if(PlayerY > yPos && rand.nextBoolean()) {
					yPos += delta/15;
				}
				else if(PlayerY < yPos && rand.nextBoolean()) {
					yPos -= delta/15;
				}
			}
		}
	}
	
	public boolean checkDead(){
		if(Life <= 0 && hurtAnim.isStopped()) {
			dead = true;
		}
		return dead;
	}
	
	public void draw() {
		if(!dead) {
			Image.draw((int)xPos, (int)yPos, 1);
		}
		hurtAnim.draw((int)xPos, (int)yPos);
	}

}
