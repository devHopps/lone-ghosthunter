package me.hopps.alone;


import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.tiled.TiledMap;
import org.newdawn.slick.Input;


public class MainGame extends BasicGame {
	
	Input Input;
	TiledMap Map;
	Player Player;
	ArrayList<Enemy> Enemies = new ArrayList<Enemy>();
	boolean[][] Walls;
	Hud Hud;
	boolean started = false, gameOver = false;
	Soundplayer Sound;
	int healing, StartX, StartY;
 
    public MainGame()
    {
        super("Lone Ghosthunter");
    }
    
    public Player getPlayer() {
    	return Player;
    }
    
    @Override
	public void init(GameContainer gc) throws SlickException {
    	gc.setShowFPS(false);
    	Sound = new Soundplayer();
    	
    	gc.setVSync(true);
    	gc.setSmoothDeltas(true);
    	Input = new Input(800);
    	
		Map = new TiledMap("res/Level.tmx", "res");
		//Get Object-Properties and set up arrays
		Walls = new boolean[Map.getHeight()][Map.getWidth()];
		for(int x = 0; x < Map.getHeight(); x++) {
			for(int y = 0; y < Map.getHeight(); y++) {
				for(int z = 0; z < Map.getLayerCount(); z++) {
					if("true".equals(Map.getTileProperty(Map.getTileId(x,y,z), "blocked", "true"))) {
						 Walls[x][y] = true;
					 }
					if("true".equals(Map.getTileProperty(Map.getTileId(x,y,z), "starter", "true"))) {
						 StartX = x;
						 StartY = y;
					 }
				}
			}
		}
		Player = new Player(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), StartX, StartY, Sound);
		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 140, 140, 5));
		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 400, 220, 5));
		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 335, 210, 5));
		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 5, 128, 5));
		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 512, 123, 5));
		
		Hud = new Hud(this);
	}
    
    public void reinit(GameContainer gc) throws SlickException {
    	started = false;
    	gameOver = false;
    	Enemies.clear();
    	healing = 0;
		Player = new Player(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), StartX, StartY, Sound);
		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 140, 140, 5));
		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 400, 220, 5));
		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 335, 210, 5));
		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 5, 128, 5));
		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 512, 123, 5));
		
		Hud.reinit();
	}
    
    public void addRandomly() throws SlickException {
    	if(started && !gameOver) {
	    	Random rand = new Random();
	    	Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 5));
	    	if(Hud.Sekunden > 20) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 5));
	    	} else if(Hud.Sekunden > 40) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 5));
	    	}else if(Hud.Sekunden > 50) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 5));
	    	}else if(Hud.Sekunden > 60) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 10));
	    	}else if(Hud.Sekunden > 80) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 10));
	    	}else if(Hud.Sekunden > 100) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 10));
	    	}else if(Hud.Sekunden > 120) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 10));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 5));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 5));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 10));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 5));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 5));
	    	}else if(Hud.Sekunden > 140) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 15));
	    	}else if(Hud.Sekunden > 160) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 15));
	    	}else if(Hud.Sekunden > 180) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 20));
	    	}else if(Hud.Sekunden > 200) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 20));
	    	}else if(Hud.Sekunden > 220) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 20));
	    	}else if(Hud.Sekunden > 240) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 20));
	    	}else if(Hud.Sekunden > 260) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 20));
	    	}else if(Hud.Sekunden > 280) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 20));
	    	}else if(Hud.Sekunden > 300) {
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 100));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 100));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 100));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 100));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 100));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 100));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 100));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 100));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 100));
	    		Enemies.add(new Enemy(new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16), 25+rand.nextInt(750), 25+rand.nextInt(550), 100));
	    	}
    	}
    }
    
    @Override
	public void update(GameContainer gc, int delta) throws SlickException {
    	if (started && !gameOver) {
    		
    		Hud.update();
    		
			for(int i = 0; i < Enemies.size(); i++) {
				Enemies.get(i).update(delta, Player.xPos, Player.yPos);
				if(!Enemies.get(i).checkDead() && Player.xPos - Enemies.get(i).xPos > -8 && Player.xPos - Enemies.get(i).xPos < 8  && Player.yPos - Enemies.get(i).yPos > -8 && Player.yPos - Enemies.get(i).yPos < 8) {
					Player.hurt(10);
				}
				if(Enemies.get(i).checkDead()) {
					Enemies.remove(i);
					Hud.addKilled();
					healing++;
					if(Player.Life < 90 && healing >= 10) {
						Player.Life +=10;
						healing -= 10;
						Sound.play("heal");
					}
				}
			}
			
			if(Player.Life <= 0) {
				gameOver = true;
				Sound.play("gameOver");
			}
			
			if(Input.isKeyDown(Input.KEY_RIGHT) || Input.isKeyDown(Input.KEY_D)) {
				 if (!isBlocked(Player.getX() + 16 + delta /8, Player.getY())) {
					Player.moveRight(delta);
				}
			}
		
			if(Input.isKeyDown(Input.KEY_LEFT) || Input.isKeyDown(Input.KEY_A)) {
				if (!isBlocked(Player.getX() - delta / 8, Player.getY())) {
					Player.moveLeft(delta);
				}
			}
			
			if(Input.isKeyDown(Input.KEY_UP) || Input.isKeyDown(Input.KEY_W)) {
				if (!isBlocked(Player.getX(), Player.getY() - delta / 8))
					Player.moveUp(delta);
		
			}
			
			if(Input.isKeyDown(Input.KEY_DOWN) || Input.isKeyDown(Input.KEY_S)) {
				if (!isBlocked(Player.getX(), Player.getY() + 16 + delta / 8)) {
					Player.moveDown(delta);
				}
			}
			
			if(Input.isKeyDown(Input.KEY_SPACE)) {
				Player.doDmg(Enemies);
			}
    	
    	} else if (!started && !gameOver){
    		if(Input.isKeyDown(Input.KEY_G)) {
    			started = true;
			}
    	} else {
    		if(Input.isKeyDown(Input.KEY_R)) {
    			this.reinit(gc);
    		}
    	}
    	
	}


    private boolean isBlocked(double d, double e)
    {
    	int xBlock = (int)d / 16;
    	int yBlock = (int)e / 16;
    	return Walls[xBlock][yBlock];
    }

	@Override
	public void render(GameContainer gc, Graphics delta) throws SlickException {
		Map.render(0,0);
		Player.draw();
		for(int i = 0; i < Enemies.size(); i++) {
			Enemies.get(i).draw();
		}
		Hud.draw();
		if(!started) {
			Hud.showStartMessage();
		}
		if(gameOver) {
			Hud.showGameOver();
		}
	}
    
}