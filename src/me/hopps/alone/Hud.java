package me.hopps.alone;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.Image;

@SuppressWarnings("deprecation")
public class Hud {
	
	long time;
	int Sekunden, killed;
	TrueTypeFont ttFont, StartFont, EndFont;
	MainGame Main;
	SpriteSheet Sprites;
	Image Heart, Kitten, Background;
	
	public Hud(MainGame pMain) throws SlickException {
    	Font awtFont = new Font("Times New Roman", Font.BOLD, 12);
    	 ttFont = new TrueTypeFont(awtFont, true);
    	 
    	 Font awt2Font = new Font("Times New Roman", Font.BOLD, 44);
    	 StartFont = new TrueTypeFont(awt2Font, true);
    	 
    	 Font awt3Font = new Font("Times New Roman", Font.BOLD, 24);
    	 EndFont = new TrueTypeFont(awt3Font, true);
    	 
    	 
    	 Main = pMain;
    	 
    	 Sprites = new SpriteSheet("res/spritesheets/EntitySprites.png", 16, 16);
    	 
    	 Background = new Image("res/spritesheets/background.png");
    	 Heart = Sprites.getSprite(1, 4);
    	 Kitten = Sprites.getSprite(2, 4);
	}
	
	public void reinit() {
		Sekunden = 0;
		killed = 0;
	}
	
	public void update() {
		if(time+1000000000 <= System.nanoTime()) {
    		Sekunden++;
    		time = System.nanoTime();
    		try {
				Main.addRandomly();
			} catch (SlickException e) {
				e.printStackTrace();
			}
    	}
	}
	
	public void addKilled() {
		killed++;
	}
	
	public void draw() {
		ttFont.drawString(10f, 0f, Sekunden +" seconds", Color.red);
		
		Player Player = Main.getPlayer();
		if(Player.Life > 0) {
			Heart.draw(10, 565, 2);
		}
		if(Player.Life > 10) {
			Heart.draw(42, 565, 2);
		}
		if(Player.Life > 20) {
			Heart.draw(74, 565, 2);
		}
		if(Player.Life > 30) {
			Heart.draw(106, 565, 2);
		}
		if(Player.Life > 40) {
			Heart.draw(138, 565, 2);
		}
		if(Player.Life > 50) {
			Heart.draw(170, 565, 2);
		}
		if(Player.Life > 60) {
			Heart.draw(202, 565, 2);
		}
		if(Player.Life > 70) {
			Heart.draw(234, 565, 2);
		}
		if(Player.Life > 80) {
			Heart.draw(266, 565, 2);
		}
		if(Player.Life > 90) {
			Heart.draw(298, 565, 2);
		}
	}

	public void showStartMessage() {
		Background.draw(0, 150);
		StartFont.drawString(220f, 200f, "Press 'G' to start!", Color.red);
		ttFont.drawString(220f, 250f, "You are on your own against those ghosts.", Color.green);
		ttFont.drawString(220f, 270f, "Try to survive as long as possible.", Color.green);
		ttFont.drawString(220f, 290f, "Use 'W','A','S','D' to move and 'space' to use your sword.", Color.green);
		ttFont.drawString(220f, 310f, "Hint#1: You heal 10 HP when you kill 10 ghosts!", Color.green);
		ttFont.drawString(220f, 330f, "Hint#2: You only get damage once a second!", Color.green);
		ttFont.drawString(220f, 350f, "Hint#3: You may win special trophies!", Color.green);
		ttFont.drawString(220f, 370f, "Copyright Benedict Balzer; www.hopps.me", Color.green);
	}
	
	public void showGameOver() {
		Background.draw(0, 150);
		StartFont.drawString(255f, 150f, "Game Over!", Color.red);
		StartFont.drawString(220f, 200f, "Press 'R' to restart", Color.red);
		ttFont.drawString(220f, 250f, "You survived " + Sekunden + " seconds" , Color.green);
		ttFont.drawString(220f, 270f, "You killed " + killed + " ghosts" , Color.green);
		if(killed < 10) {
			EndFont.drawString(220f, 310f, "Did you read the instructions?", Color.red);
		} 
		if(killed >= 10 && killed < 30) {
			EndFont.drawString(220f, 310f, "You received a trophy", Color.red);
			EndFont.drawString(220f, 340f, "Worst Ghosthunter of the year!", Color.red);
		} 
		if(killed >= 30 && killed < 50) {
			EndFont.drawString(220f, 310f, "You received a trophy", Color.red);
			EndFont.drawString(220f, 340f, "Ghost Trophy!(You can't see it, haha)", Color.red);
		} 
		if(killed >= 50 && killed < 80) {
			EndFont.drawString(220f, 310f, "You received a trophy", Color.red);
			EndFont.drawString(220f, 340f, "Best Ghosthunter of the second!", Color.red);
		} 
		if(killed >= 80 && killed < 100) {
			EndFont.drawString(220f, 310f, "You received a trophy", Color.red);
			EndFont.drawString(220f, 340f, "Best Ghosthunter of the day!", Color.red);
		} 
		if(killed >= 100 && killed < 150) {
			EndFont.drawString(220f, 310f, "You received a trophy", Color.red);
			EndFont.drawString(220f, 350f, "Best Ghosthunter of the year!", Color.red);
		} 
		if(killed >= 150) {
			EndFont.drawString(220f, 310f, "You received a trophy", Color.red);
			EndFont.drawString(220f, 340f, "Kitten Trophy!", Color.red);
			Kitten.draw(310, 360, 6);
		}
	}

}
