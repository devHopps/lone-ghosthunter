package me.hopps.alone;

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.Image;

public class Player {
	
	SpriteSheet Sprites;
	double xPos, yPos, steps;
	String MoveDirection;
	long lastShoot;
	Animation SwordRight, SwordLeft, SwordDown, SwordUp;
	long LastTimeHurt;
	
	boolean doDmg;
	
	int Life = 100;
	int SwortDmg = 3;
	Soundplayer Sound;

	
	Image Image;

	
	public Player(SpriteSheet ySprites,int xxPos,int yyPos, Soundplayer pSound) {
		Sound = pSound;
		
		MoveDirection = "right";
		Sprites = ySprites;
		
		Image = Sprites.getSprite(0, 0);
		
		xPos = xxPos*16;
		yPos = yyPos*16;
		
		SwordRight = new Animation(Sprites, 3, 0, 3, 4, true, 120, true);
		SwordRight.setLooping(false);

		SwordLeft = new Animation(Sprites, 4, 0, 4, 4, true, 120, true);
		SwordLeft.setLooping(false);
		
		SwordDown = new Animation(Sprites, 5, 0, 5, 4, true, 120, true);
		SwordDown.setLooping(false);
		
		SwordUp = new Animation(Sprites, 6, 0, 6, 4, true, 120, true);
		SwordUp.setLooping(false);

	}
	
	public void moveRight(int delta) {
		xPos = (xPos+delta/6);
		MoveDirection = "right";
		steps++;
	}
	
	public void moveLeft(int delta) {
		xPos = (xPos-delta/6);
		MoveDirection = "left";
		steps++;
	}
	
	public void moveUp(int delta) {
		yPos = (yPos-delta/6);
		MoveDirection = "up";
		steps++;
	}
	
	public void moveDown(int delta) {
		yPos = (yPos+delta/6);
		MoveDirection = "down";
		steps++;
	}
	
	
	public double getX() {
		return xPos;
	}
	
	public double getY() {
		return yPos;
	}
	
	public void doDmg(ArrayList<Enemy> Enemies) {
		if(MoveDirection == "right" && SwordRight.getFrame() > 3 && SwordLeft.getFrame() > 3 && SwordDown.getFrame() > 3 && SwordDown.getFrame() > 3 && SwordUp.getFrame() > 3) {
			SwordRight.restart();
			Sound.play("WeaponUse");
		}
		if(MoveDirection == "left" && SwordRight.getFrame() > 3 && SwordLeft.getFrame() > 3 && SwordDown.getFrame() > 3 && SwordDown.getFrame() > 3 && SwordUp.getFrame() > 3) {
			SwordLeft.restart();
			Sound.play("WeaponUse");
		}
		if(MoveDirection == "down" && SwordRight.getFrame() > 3 && SwordLeft.getFrame() > 3 && SwordDown.getFrame() > 3 && SwordDown.getFrame() > 3 && SwordUp.getFrame() > 3) {
			SwordDown.restart();
			Sound.play("WeaponUse");
		}
		if(MoveDirection == "up" && SwordRight.getFrame() > 3 && SwordLeft.getFrame() > 3 && SwordDown.getFrame() > 3 && SwordDown.getFrame() > 3 && SwordUp.getFrame() > 3) {
			SwordUp.restart();
			Sound.play("WeaponUse");
		}
		
		for(int i = 0; i < Enemies.size(); i++) {
			if(xPos - Enemies.get(i).xPos > -20 && xPos - Enemies.get(i).xPos < 0 && MoveDirection == "right" && yPos - Enemies.get(i).yPos > -10 && yPos - Enemies.get(i).yPos < 10) {
				Enemies.get(i).hurt(SwortDmg, MoveDirection);
				Sound.play("hit");
			}
			else if(xPos - Enemies.get(i).xPos < 20 && xPos - Enemies.get(i).xPos > 0 && MoveDirection == "left" && yPos - Enemies.get(i).yPos > -10 && yPos - Enemies.get(i).yPos < 10) {
				Enemies.get(i).hurt(SwortDmg, MoveDirection);
				Sound.play("hit");
			}
			else if(yPos - Enemies.get(i).yPos < 30 && yPos - Enemies.get(i).yPos > 0 &&MoveDirection == "up" && xPos - Enemies.get(i).xPos > -10 && xPos - Enemies.get(i).xPos < 10) {
				Enemies.get(i).hurt(SwortDmg, MoveDirection);
				Sound.play("hit");
			}
			else if(yPos - Enemies.get(i).yPos > -25 && yPos - Enemies.get(i).yPos < 0 && MoveDirection == "down" && xPos - Enemies.get(i).xPos > -10 && xPos - Enemies.get(i).xPos < 10) {
				Enemies.get(i).hurt(SwortDmg, MoveDirection);
				Sound.play("hit");
			}
		}
	}
	
	public void paintSword() {
		SwordRight.draw((int)xPos+7, (int)yPos);
		SwordLeft.draw((int)xPos-9, (int)yPos);
		SwordDown.draw((int)xPos, (int)yPos+7);
		SwordUp.draw((int)xPos, (int)yPos-5);
	}
	
	public void hurt(int dmg) {
		if(LastTimeHurt+1000000000 < System.nanoTime()) {
			Life -= dmg;
			LastTimeHurt = System.nanoTime();
			Sound.play("hurt");
		}
	}
	
	public void draw() {
		int row = 0;
		if(MoveDirection == "right") {
			row = 0;
		}
		if(MoveDirection == "left") {
			row = 1;
		}
		if(MoveDirection == "down") {
			row = 2;
		}
		if(MoveDirection == "up") {
			row = 3;
		}
		
		if(steps <= 10) {
			Image = Sprites.getSprite(0, row);
		} else if (steps <= 20 && steps > 10) {
			Image = Sprites.getSprite(1, row);
		} else if (steps <= 30 && steps > 20) {
			Image = Sprites.getSprite(0, row);
		}else if (steps <= 40 && steps > 30) {
			Image = Sprites.getSprite(2, row);
		} else {
			steps = 0;
		}

		Image.draw((int)xPos, (int)yPos, 1);
		
		paintSword();

		
	}

}
